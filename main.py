import vector


from matrix import Matrix, zeroes, identity

v1 = vector.Vector([8.218,-9.341])
v2 = vector.Vector([-1.129,2.111])

print (v1+v2)

print (vector.Vector([7.119,8.215]) - vector.Vector([-8.223,0.878]))

print (vector.Vector([1.671,-1.012,-0.318]).scalar(7.41))

print (vector.Vector([-0.221, 7.437]).magnitude())
print (vector.Vector([8.813, -1.331, -6.247]).magnitude())

print (vector.Vector([5.581, -2.136]).normalize())
print (vector.Vector([1.996,3.108,-4.554]).normalize())

print (vector.Vector([7.887,4.138]).dotproduct(vector.Vector([-8.802,6.776])))
print (vector.Vector([-5.955,-4.904,-1.874]).dotproduct(vector.Vector([-4.496,-8.755,7.103])))


m1 = Matrix([
    [1, 2],
    [3, 4]
])

m2 = Matrix([
    [2, 5],
    [6, 1]
])



print("m1 is")
print(m1)

print("m2 is")
print(m2)

m3 = m1 + m2
print("m1 + m2 is")
print(m3)

m4 = -m1
print(m4)

m3 = m3-m2
print(m3)

m4 = 2*m3
print(m4)

I2 = Matrix([
    [1, 0],
    [0, 1]
    ])
I2_neg = Matrix([
    [-1, 0],
    [0, -1]
    ])

print(-I2)

print (I2+I2_neg)

m1 = Matrix([
        [1,2,3],
        [4,5,6]
        ])

m2 = Matrix([
    [7,-2],
    [-3,-5],
    [4,1]
    ])

m1_x_m2 = Matrix([

    [ 13,  -9],
    [ 37, -27]])


m6 = m1*m2
print (m6)


top_ones = Matrix([
    [1,1],
    [0,0],
    ])

left_ones = Matrix([
    [1,0],
    [1,0]
    ])


print(top_ones.T())

t = Matrix([
    [2,-3],
    [1, 5]
])

print (t.determinant())

print (t.inverse())

print (4*identity(5).trace())
