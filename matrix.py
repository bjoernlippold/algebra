import math
from math import sqrt
import numbers
import vector

def zeroes(height, width):
        """
        Creates a matrix of zeroes.
        """
        g = [[0.0 for _ in range(width)] for __ in range(height)]
        return Matrix(g)

def identity(n):
        """
        Creates a n x n identity matrix.
        """
        I = zeroes(n, n)
        for i in range(n):
            I.g[i][i] = 1.0
        return I

class Matrix(object):

    # Constructor
    def __init__(self, grid):
        self.g = grid
        self.h = len(grid)
        self.w = len(grid[0])

    #
    # Primary matrix math methods
    #############################
 
    def determinant(self):
        """
        Calculates the determinant of a 1x1 or 2x2 matrix.
        """
        if not self.is_square():
            raise(ValueError, "Cannot calculate determinant of non-square matrix.")
        if self.h > 2:
            raise(NotImplementedError, "Calculating determinant not implemented for matrices largerer than 2x2.")
        
        if self.h==1:
            return 1/self.g[0][0]

        if self.h==2:
            return self.g[0][0]*self.g[1][1] - self.g[0][1]*self.g[1][0]

    def trace(self):
        """
        Calculates the trace of a matrix (sum of diagonal entries).
        """
        if not self.is_square():
            raise(ValueError, "Cannot calculate the trace of a non-square matrix.")

        res = 0
        for i in range(self.w):
            res+=self.g[i][i]

        return res

    def inverse(self):
        """
        Calculates the inverse of a 1x1 or 2x2 Matrix.
        """
        if not self.is_square():
            raise(ValueError, "Non-square Matrix does not have an inverse.")
        if self.h > 2:
            raise(NotImplementedError, "inversion not implemented for matrices larger than 2x2.")

        if self.h==1:
            res = zeroes(1,1)
            res[0][0] = 1/self.g[0][0]
            return res

        if self.h==2:
            a = self.g[0][0]
            b = self.g[0][1]
            c = self.g[1][0]
            d = self.g[1][1]
            invers = Matrix([[d, -b],[-c,a]])
            return (1/self.determinant())*invers


    def T(self):
        """
        Returns a transposed copy of this Matrix.
        """
        matrix_transpose = []
        # Loop through columns on outside loop
        for c in range(self.h):
            new_row = []
            # Loop through rows on inner loop
            for r in range(self.w):
                # Column values will be filled by what were each row before
                new_row.append(self.g[r][c])
            matrix_transpose.append(new_row)
        
        return Matrix(matrix_transpose)

    def is_square(self):
        return self.h == self.w

    #
    # Begin Operator Overloading
    ############################
    def __getitem__(self,idx):
        """
        Defines the behavior of using square brackets [] on instances
        of this class.

        Example:

        > my_matrix = Matrix([ [1, 2], [3, 4] ])
        > my_matrix[0]
          [1, 2]

        > my_matrix[0][0]
          1
        """
        return self.g[idx]

    def __repr__(self):
        """
        Defines the behavior of calling print on an instance of this class.
        """
        s = ""
        for row in self.g:
            s += " ".join(["{} ".format(x) for x in row])
            s += "\n"
        return s

    def __add__(self,other):
        """
        Defines the behavior of the + operator
        """
        if self.h != other.h or self.w != other.w:
            raise(ValueError, "Matrices can only be added if the dimensions are the same") 

        # iterate through rows
        result = zeroes(self.h, self.w)
        for i in range(self.h):
           # iterate through columns
           for j in range(self.w):
               result[i][j] = self[i][j] + other[i][j]

        return result

    def __neg__(self):
        """
        Defines the behavior of - operator (NOT subtraction)

        Example:

        > my_matrix = Matrix([ [1, 2], [3, 4] ])
        > negative  = -my_matrix
        > print(negative)
          -1.0  -2.0
          -3.0  -4.0
        """
        result = zeroes(self.h, self.w)
        for i in range(self.h):
           # iterate through columns
           for j in range(self.w):
               result[i][j] = -self[i][j] 

        return result

    def __sub__(self, other):
        """
        Defines the behavior of - operator (as subtraction)
        """
        result = self + (-other)
        return result

    def __mul__(self, other):
        """
        Defines the behavior of * operator (matrix multiplication)
        """
        m_rows = self.h
        p_columns = other.w
        result = zeroes(m_rows, p_columns)

        for r in range(m_rows):
            row = vector.Vector(self.g[r])
            res = 0
            for c in range(p_columns):
                colB = vector.Vector(other.get_column(c))
                result[r][c] = row.dotproduct(colB)
        return result

    def __rmul__(self, other):
        """
        Called when the thing on the left of the * is not a matrix.

        Example:

        > identity = Matrix([ [1,0], [0,1] ])
        > doubled  = 2 * identity
        > print(doubled)
          2.0  0.0
          0.0  2.0
        """
        if isinstance(other, numbers.Number):
            pass
            result = zeroes(self.h, self.w)
            for i in range(self.h):
            # iterate through columns
                for j in range(self.w):
                    result[i][j] = self[i][j] * other 

            return result


    def get_column(self, column_number):
        column = []
        for r in range(self.h):
            column.append(self.g[r][column_number])
        return column
