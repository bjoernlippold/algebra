import math


class Vector(object):
    def __init__(self, coordinates):
        try:
            if not coordinates:
                raise ValueError
            self.coordinates = tuple(coordinates)
            self.dimension = len(coordinates)

        except ValueError:
            raise ValueError('The coordinates must be nonempty')

        except TypeError:
            raise TypeError('The coordinates must be an iterable')

    def __str__(self):
        return 'Vector: {}'.format(self.coordinates)

    def __eq__(self, v):
        return self.coordinates == v.coordinates

    def __add__(self, other):
        return Vector(list([a + b for a, b in zip(self.coordinates, other.coordinates)]))

    def __sub__(self, other):
        return Vector(list([a - b for a, b in zip(self.coordinates, other.coordinates)]))

    def scalar(self, scale):
        coords = [float(scale) * float(a) for a in (self.coordinates)]
        return Vector(coords)

    def magnitude(self):
        mag = 0
        n = self.dimension
        for i in range(n):
            mag += self.coordinates[i]*self.coordinates[i]
        return math.sqrt(mag)

    def normalize(self):
        try:
            return self.scalar(1/self.magnitude())
        except ZeroDivisionError:
            raise Exception('cannot normalize zero vector')

    def dotproduct(self, other):
       return sum([x*y for x,y in zip(self.coordinates, other.coordinates)])